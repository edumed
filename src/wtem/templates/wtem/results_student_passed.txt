{% load l10n %}Witaj,

Informujemy, że wszystkie testy rozwiązane w ramach II etapu III edycji Wielkiego Turnieju Edukacji Medialnej zostały ocenione. 

Liczba punktów zdobytych przez Ciebie w teście to {% localize on %}{{final_result}}{% endlocalize %}.

Wynik ten uprawnia Cię do wzięcia udziału III etapie Turnieju. Gratulujemy! Zawody finałowe odbędą się w drugiej połowie kwietnia. Niebawem prześlemy Ci szczegółowe informacje na ten temat.

W razie jakichkolwiek pytań prosimy o kontakt.

Pozdrawiamy,
Zespół Edukacji Medialnej
fundacja Nowoczesna Polska
