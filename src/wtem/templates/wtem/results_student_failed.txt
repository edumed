{% load l10n %}Witaj,

Informujemy, że wszystkie testy rozwiązane w ramach II etapu III edycji Wielkiego Turnieju Edukacji Medialnej zostały ocenione. 

Liczba punktów zdobytych przez Ciebie w teście to {% localize on %}{{final_result}}{% endlocalize %}.

Niestety, ten wynik nie uprawnia Cię do wzięcia udziału w III etapie Turnieju. Liczba punktów, która uprawnia do wzięcia udziału w finale to 52. Zachęcamy do zgłębiania wiedzy za zakresu edukacji medialnej i wzięcia udziału w kolejnej edycji Wielkiego Turnieju Edukacji Medialnej.

W razie jakichkolwiek pytań prosimy o kontakt.

Pozdrawiamy,
Zespół Edukacji Medialnej
fundacja Nowoczesna Polska
