-i https://py.mdrn.pl:8443/simple

Django>=1.6,<1.7
South>=0.7.4
django-pipeline>=1.3,<1.4
python-memcached==1.53
django-piwik
django-extensions==1.5.7  # because South
django-piston==0.2.2.1.2
#pyScss
#git+git://github.com/Kronuz/pyScss.git@d8f4da23a3c87696a75b3830ed4ab49b75550a93#egg=pyScss
#TODO: pyScss support, for now just install sass

jsonfield>=0.9,<0.10

django_cas>=2.1,<2.2
fnpdjango>=0.1.18,<0.2

Feedparser

django-honeypot

# Librarian
lxml==3.3.5
texml

# sponsors
pillow
django-sponsors>=1.1,<1.2

django-haystack>=2.0,<2.1
pysolr==3.2.0

pybbm>=0.14,<0.15
django-annoying==0.7.9
django-libravatar

sorl-thumbnail>=11,<12
pyyaml==3.11

django-subdomains>=2.0.4,<2.1

markdown2

mailchimp3
